apiVersion: kubeadm.k8s.io/v1beta2
kind: InitConfiguration
# BootstrapTokens is respected at `kubeadm init` time and describes a set of Bootstrap Tokens to create.
# This information IS NOT uploaded to the kubeadm cluster configmap, partly because of its sensitive nature
bootstrapTokens:
  - # Token is used for establishing bidirectional trust between nodes and control-planes.
    # Used for joining nodes in the cluster.
    token: 5jy0p0.ms6cqjlpx13ohhnw
    # TTL defines the time to live for this token. Defaults to 24h.
    # Expires and TTL are mutually exclusive.
    ttl: 2h0m0s
    # Usages describes the ways in which this token can be used. Can by default be used
    # for establishing bidirectional trust, but that can be changed here.
    usages:
      - signing
      - authentication
    # Groups specifies the extra groups that this token will authenticate as when/if
    # used for authentication
    groups:
      - system:bootstrappers:kubeadm:default-node-token
# NodeRegistration holds fields that relate to registering the new control-plane node to the cluster
nodeRegistration:
  # Taints specifies the taints the Node API object should be registered with. If this field is unset, i.e. nil, in the `kubeadm init` process
  #	it will be defaulted to []v1.Taint{'node-role.kubernetes.io/master=""'}. If you don't want to taint your control-plane node, set this field to an
  #	empty slice, i.e. `taints: []` in the YAML file. This field is solely used for Node registration.
  taints: []
# LocalAPIEndpoint represents the endpoint of the API server instance that's deployed on this control plane node
# In HA setups, this differs from ClusterConfiguration.ControlPlaneEndpoint in the sense that ControlPlaneEndpoint
# is the global endpoint for the cluster, which then loadbalances the requests to each individual API server. This
# configuration object lets you customize what IP/DNS name and port the local API server advertises it's accessible
# on. By default, kubeadm tries to auto-detect the IP of the default interface and use that, but in case that process
# fails you may set the desired value here
localAPIEndpoint:
  # AdvertiseAddress sets the IP address for the API server to advertise.
  advertiseAddress: 10.0.5.10
  # BindPort sets the secure port for the API Server to bind to.
  #	Defaults to 6443.
  bindPort: 6443
# CertificateKey sets the key with which certificates and keys are encrypted prior to being uploaded in
# a secret in the cluster during the uploadcerts init phase.
certificateKey: a3b3e346fbc0c33ba291450cf6879e15ac4636ddf7041b8cb9163a050dd968f5
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: JoinConfiguration
# NodeRegistration holds fields that relate to registering the new control-plane node to the cluster
nodeRegistration:
  # Taints specifies the taints the Node API object should be registered with. If this field is unset, i.e. nil, in the `kubeadm init` process
  #	it will be defaulted to []v1.Taint{'node-role.kubernetes.io/master=""'}. If you don't want to taint your control-plane node, set this field to an
  #	empty slice, i.e. `taints: []` in the YAML file. This field is solely used for Node registration.
  taints: []
# Discovery specifies the options for the kubelet to use during the TLS Bootstrap process
discovery:
  # BootstrapToken is used to set the options for bootstrap token based discovery
  #	BootstrapToken and File are mutually exclusive
  bootstrapToken:
    # Token is a token used to validate cluster information
    # fetched from the control-plane.
    token: 5jy0p0.ms6cqjlpx13ohhnw
    # APIServerEndpoint is an IP or domain name to the API server from which info will be fetched.
    apiServerEndpoint: 10.0.5.10:6443
    # UnsafeSkipCAVerification allows token-based discovery
    # without CA verification via CACertHashes. This can weaken
    # the security of kubeadm since other nodes can impersonate the control-plane.
    unsafeSkipCAVerification: true
# ControlPlane defines the additional control plane instance to be deployed on the joining node.
# If nil, no additional control plane instance will be deployed.
controlPlane:
  # LocalAPIEndpoint represents the endpoint of the API server instance to be deployed on this node.
  localAPIEndpoint:
    # AdvertiseAddress sets the IP address for the API server to advertise.
    advertiseAddress: 10.0.5.11
    # BindPort sets the secure port for the API Server to bind to.
    #	Defaults to 6443.
    bindPort: 6443
  # CertificateKey is the key that is used for decryption of certificates after they are downloaded from the secret
  #	upon joining a new control plane node. The corresponding encryption key is in the InitConfiguration.
  certificateKey: a3b3e346fbc0c33ba291450cf6879e15ac4636ddf7041b8cb9163a050dd968f5
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
# Etcd holds configuration for etcd.
etcd:
  # Local provides configuration knobs for configuring the local etcd instance
  #	Local and External are mutually exclusive
  local:
    dataDir: /var/lib/etcd
    # ExtraArgs are extra arguments provided to the etcd binary
    # when run inside a static pod.
    #extraArgs:
    #  advertise-client-urls: https://10.0.5.10:2379
    #  listen-client-urls: https://10.0.5.10:2379,https://127.0.0.1:2379
    #  initial-advertise-peer-urls: https://10.0.5.10:2380
    #  listen-peer-urls: https://10.0.5.10:2380
      # todo
# Networking holds configuration for the networking topology of the cluster.
networking:
  # ServiceSubnet is the subnet used by k8s services. Defaults to "10.96.0.0/12".
  serviceSubnet: 10.96.0.0/12
  # PodSubnet is the subnet used by pods.
  podSubnet: 10.200.0.0/16
  # DNSDomain is the dns domain used by k8s services. Defaults to "cluster.local".
  dnsDomain: cluster.local
# KubernetesVersion is the target version of the control plane.
kubernetesVersion: v1.17.0
# ControlPlaneEndpoint sets a stable IP address or DNS name for the control plane; it
# can be a valid IP address or a RFC-1123 DNS subdomain, both with optional TCP port.
# In case the ControlPlaneEndpoint is not specified, the AdvertiseAddress + BindPort
# are used; in case the ControlPlaneEndpoint is specified but without a TCP port,
# the BindPort is used.
# Possible usages are:
# e.g. In a cluster with more than one control plane instances, this field should be
# assigned the address of the external load balancer in front of the
# control plane instances.
# e.g.  in environments with enforced node recycling, the ControlPlaneEndpoint
# could be used for assigning a stable DNS to the control plane.
controlPlaneEndpoint: "10.0.5.10"
# APIServer contains extra settings for the API server control plane component
apiServer:
  # ExtraArgs is an extra set of flags to pass to the control plane component.
  #	use ComponentConfig + ConfigMaps.
  extraArgs:
    authorization-mode: Node,RBAC
  # TimeoutForControlPlane controls the timeout that we use for API server to appear
  timeoutForControlPlane: 4m0s
# DNS defines the options for the DNS add-on installed in the cluster.
dns:
  # Type defines the DNS add-on to be used
  type: CoreDNS
# CertificatesDir specifies where to store or look for all required certificates.
certificatesDir: /etc/kubernetes/pki
# ImageRepository sets the container registry to pull images from.
# If empty, `k8s.gcr.io` will be used by default; in case of kubernetes version is a CI build (kubernetes version starts with `ci/` or `ci-cross/`)
# `gcr.io/kubernetes-ci-images` will be used as a default for control plane components and for kube-proxy, while `k8s.gcr.io`
# will be used for all the other images.
imageRepository: k8s.gcr.io
# The cluster name
clusterName: kub3rn3t3s